<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 6-2-2018
 * Time: 20:30
 */

require_once 'Core/init.php';

/**
 * Dispatching the world
 */

$homeController = new homeController();

if(isset($_GET['action']) && $_GET['action'] != "") {
    if($_GET['action'] == "add") {
        $homeController->showAdd();
    }
} else if(isset($_GET['action']) && isset($_GET['id']) && $_GET['action'] != "" && $_GET['id'] != ""){
    if($_GET['action'] == "edit") {
        $homeController->showEdit();
    }
}else if(isset($_GET['action']) && isset($_GET['id']) && $_GET['action'] != "" && $_GET['id'] != ""){
    if($_GET['action'] == "detail") {
        $homeController->showDetails();
    }
}else if(isset($_GET['action']) && isset($_GET['id']) && $_GET['action'] != "" && $_GET['id'] != ""){
    if($_GET['action'] == "delete") {
        $homeController->delete();
    }
} else {
    $homeController->showHome();
}