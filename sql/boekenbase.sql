-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Gegenereerd op: 20 feb 2018 om 19:29
-- Serverversie: 5.7.19
-- PHP-versie: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boekenbase`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `auteurs`
--

DROP TABLE IF EXISTS `auteurs`;
CREATE TABLE IF NOT EXISTS `auteurs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `a_voornaam` varchar(255) NOT NULL,
  `a_achternaam` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `auteurs`
--

INSERT INTO `auteurs` (`id`, `a_voornaam`, `a_achternaam`) VALUES
(1, 'Annelies', 'Verbeke'),
(2, 'Bart', 'Moeyaert'),
(4, 'Bart', 'Van Loo'),
(5, 'Bob', 'Mendes'),
(6, 'Brigitte', 'Minne'),
(7, 'David', 'Van Reybrouck'),
(8, 'Dirk', 'Nielandt'),
(9, 'Eef', 'Lanoye'),
(10, 'Erwin', 'Mortier'),
(11, 'Griet ', 'op de Beeck'),
(12, 'Jeroen ', 'Brouwers');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `titels`
--

DROP TABLE IF EXISTS `titels`;
CREATE TABLE IF NOT EXISTS `titels` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `titel` varchar(255) NOT NULL,
  `auteurs_id` int(10) NOT NULL,
  `uitgevers_id` int(10) NOT NULL,
  `omschrijving` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `uitgeleend` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `titels`
--

INSERT INTO `titels` (`id`, `titel`, `auteurs_id`, `uitgevers_id`, `omschrijving`, `image`, `comment`, `uitgeleend`) VALUES
(1, 'Ben je bang in het bos, Grote Wolf?	', 11, 22, '', '', '', 0),
(2, 'Ik zie je op het strand', 18, 9, '', '', '', 0),
(3, 'De ruïnes van Gorlan', 1, 6, '', '', '', 0),
(4, 'Matilda', 5, 7, '', '', '', 0),
(5, 'In het water', 16, 22, '', '', '', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `uitgevers`
--

DROP TABLE IF EXISTS `uitgevers`;
CREATE TABLE IF NOT EXISTS `uitgevers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uitgeversnaam` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `uitgevers`
--

INSERT INTO `uitgevers` (`id`, `uitgeversnaam`) VALUES
(1, 'Abimo'),
(2, 'Acco'),
(3, 'Arcadiastrips'),
(4, 'Artus Uitgevers'),
(5, 'Averbode'),
(6, 'Ballon Media'),
(7, 'Borgerhoff & Lamberigts'),
(8, 'Clavis'),
(9, 'Daedalus'),
(10, 'Davidsfonds'),
(11, 'De Draak Vzw'),
(12, 'De Eenhoorn'),
(13, 'De Vries- Brouwers Antwerpen'),
(14, 'Die Keure Brugge'),
(15, 'Dupuis'),
(16, 'EPO'),
(17, 'Horizon'),
(18, 'Houtekiet'),
(19, 'Intersentia'),
(20, 'Kannibaal'),
(21, 'Kramat'),
(22, 'Lannoo');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
