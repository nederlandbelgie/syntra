
<div class="contained">
    <div class="row">
        <div class="col">
            <h2 class="text-center">Voer een nieuw boek in</h2>
        </div>
    </div>

    <?php

    ?>


    <div class="row">
        <form class="offset-md-2 col-md-8" method="POST" action="add">
            <div class="form-group">
                <label for="title">Titel</label>
                <input type="text" class="form-control" id="title" placeholder="Titel">
            </div>
            <div class="form-group">
                <label for="f_author">Voornaam Auteur</label>
                <input type="text" class="form-control" id="f_author" placeholder="Voornaam Auteur">
            </div>
            <div class="form-group">
                <label for="l_author">Achternaam Auteur</label>
                <input type="text" class="form-control" id="l_author" placeholder="Achternaam Auteur">
            </div>
            <div class="form-group">
                <label for="publisher">Uitgever</label>
                <input list="select" class="form-control col-md-12">
                <datalist id="select" class="col-md-12">
                    <option value="test">test</option>
                </datalist>
            </div>
            <div class="form-group">
                <label for="description">Omschrijving</label>
                <textarea class="form-control" id="description" rows="5"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Invoeren</button>
        </form>
    </div>
</div>
