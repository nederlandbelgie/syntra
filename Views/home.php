<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 6-2-2018
 * Time: 20:33
 */

?>

<div class="contained">
    <div class="row">
        <div class="col">
            <h1 class="h2">Overzicht</h1>
        </div>
    </div>
    <form class="row">
        <div class="ml-5 col-5">
            <div class="form-group row">
                <label for="inputEmail3" class="col-md-1 mr-3 col-form-label">Title</label>
                <div class="col-sm-10">
                    <input list="title" type="text" class="form-control" id="bookTitle" placeholder="Title">
                    <datalist  id="title">
                        
                    </datalist>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="form-group row">
                <label for="inputEmail3" class="col-md-2 col-form-label">Auteur</label>
                <div class="col-sm-10">
                    <input list="author" type="text" class="form-control" id="auteur" placeholder="Auteur">
                    <datalist id="author">
                        
                    </datalist>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="form-group row">
                <label for="inputEmail3" class="col-md-2 col-form-label">Uitgever</label>
                <div class="col-sm-10">
                    <input list="data_publisher" type="text" class="form-control" id="uitgever" placeholder="Uitgever">
                    <datalist id="data_publisher">
                        
                    </datalist>
                </div>
            </div>
        </div>
        <div class="col">
            <input type="submit" value="Zoeken" class="btn btn-primary" style="margin-top: 7px;">
        </div>
    </form>
    <div class="row">
        <div class="col">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col" width="10">#</th>
                    <th scope="col">Title</th>
                    <th scope="col" width="200">Auteur</th>
                    <th scope="col" width="100">Uitgever</th>
                    <th scope="col">Commentaar</th>
                    <th scope="col" width="10">Uitgeleend</th>
                    <th scope="col" width="40"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $records = DB::getInstance()->getAll('titels');
                if(!$records->getCount())
                {
                    echo 'No Data';
                }
                else
                {
                    foreach($records->getResults() as $record)
                    {
                        echo '<tr>',
                            '<th scope="row">'.$record->id.'</th>',
                            '<td>'.$record->titel.'</td>',
                            '<td>'.$record->auteurs_id.'</td>',
                            '<td>'.$record->uitgevers_id.'</td>',
                            '<td>'.$record->comment.'</td>',
                            '<td>'.$record->uitgeleend.'</td>',
                        '<td>',
                        '<div class="btn-group" role="group" aria-label="Basic example">',
                            '<a href="./index.php?action=detail&id='.$record->id.'" type="button" class="btn btn-primary">Details</a>',
                            '<a href="./index.php?action=edit&id='.$record->id.'" type="button" class="btn btn-success">Edit</a>',
                            '<a href="./index.php?action=delete&id='.$record->id.'" type="button" class="btn btn-danger">Delete</a>',
                        '</div>',
                        '</td>',
                        '</tr>';
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

