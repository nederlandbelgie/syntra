Maurice
Davy
suzannevn
Lesley

Database

Auteur->id,voornaam,achternaam
Titel->id,titel,omschrijving,image auteur_id,uitgever_id,comment
Uitgever->id,naam


Userstories

Overzichtscherm

De gebruiker klikt op overzicht->Een overzicht van alle boeken worden ingeladen.
De gebruiker klikt op nieuw boek->De gebruiker gaat naar de pagina om een boek toe te voegen aan het overzicht. 
De gebruiker klikt op details op een bepaalde regel->De gebruiker gaat naar het pagina met alle details van het boek.
De gebruiker klikt op aanpassen op een bepaalde regel->De gebruiker gaat naar het pagina voor een nieuw boek toe te voegen en de aan te passen data wordt er reeds ingeladen.
De gebruiker klikt op verwijderen op een bepaalde regel -> De gebruiker krijgt een bevestiging voor het verwijderen van de toebehorende titel. 
De gebruiker selecteert een titel in het selecteermenu-> Alle titels met deze naam worden weer gegeven.
De gebruiker selecteert een auteur in het selecteermenu-> Alle titels met deze auteur worden weer gegeven.
De gebruiker selecteert een uitgever in het selecteermenu-> Alle titels met deze uitgever worden weer gegeven.
De gebruiker klikt op pagina 2(enzo verder)->Alle titels van pagina 2 worden ingeladen.

Nieuw Boek

De gebruiker klikt op titelveld -> De gebruiker kan hier een titel invoeren.
De gebruiker klikt op auteurveld -> De gebruiker kan hier een auteur invoeren.
De gebruiker klikt op uitgeverveld -> De gebruiker kan hier een uitgever invoeren.
De gebruiker klikt op beschrijvingsveld -> De gebruiker kan hier een beschrijving invoeren.
De gebruiker klikt op image veld ->De gebruiker kan hier een pad naar de image invoeren.
De gebruiker klikt op commentveld -> De gebruiker kan hier een comment invoeren.
De gebruiker klikt op uitgever veld -> De gebruiker kan hier een uitgever invoeren.
De gebruiker klikt op Invoeren -> De gegevens worden in de database opgenomen.

Details

De gebruiker klikt op overzicht->De gebruiker gaat terug naar het overzicht van alle boeken.
De gebruiker klikt op nieuw boek->De gebruiker gaat naar de pagina om een boek toe te voegen aan het overzicht.