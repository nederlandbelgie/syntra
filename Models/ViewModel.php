<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 6-2-2018
 * Time: 22:50
 */

class ViewModel
{
    private $page, $title;

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function renderHeader(){
        $GLOBALS["pageSettings"] =  ["Page" => $this->page, "Title" => $this->title];
        include_once ("Views/Layout/header.phtml");
    }

    public function pageFooter(){
        include_once ("Views/Layout/footer.phtml");
    }

    public function renderBody(){

        switch ($this->page){
            case "Home":
                include_once "Views/home.php";
                break;

            case "Add":
                include_once "Views/add.php";
                break;

            case "Details":
                include_once "Views/detail.php";
                break;

            default:
                include_once "Views/404/404.php";
                break;
        }

    }
}