<?php
/**
 * Class Details:
 *  User: Ministrare
 *  Date: 11/02/2018
 *  Time: 13:19
 *
 * Global Config Class:
 *  Transforms the global config settings into a accessible Static method
 *
 *  To access any config settings:
 *      config::get('mysql/username');
 */

class Config
{
    /**
     * @param null $path
     * @return bool|mixed
     */
    public static function get($path = null )
    {
        if( !$path )
        {
            return false;
        }

        $config = $GLOBALS['config'];
        $path = explode ( '/', $path );

        foreach( $path as $bit )
        {
            if( isset( $config[$bit] ))
            {
                $config = $config[$bit];
            }
        }

        return $config;
    }
}