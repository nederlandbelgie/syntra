
**********  Documentatie  **********

*** Models:

    ** Db uitleg:

        * Constructor:
            De constructor is verantwoordelijk voor het initaliseren van de DB connection.
            De waardes die nodig zijn voor het verbinden met de database via PDO, worden opgehaald uit een global variabel
            dat in de Core/cofig word gedifineerd en die dankzij de config class, toegang verleend word naar deze waardes via een Static method.
            Indien de connectie niet gelukt is, word het programma stilgelegt via DIE() en word er netjes een error weergegeven op het scherm.


        * getInstance():
            Deze functie dient om de verbinding op te halen.
            Indien de verbinding al eerder is aangemaakt, word deze instantie opgeheeld en terug gegeven aan de gebruiker.
            Zo word er voorkomen, dat er onnodig verbindingen worden aangemaakt waar door de server kan worden overbelast.

            Voorbeeld:

            $db_connection = DB::getInstance();


        * getResults():
            Via deze functie is het mogelijk om de opgeslagen waarden van een successvolle query uitvoering, als een object op te halen.

            Voorbeeld:

            $db_connection->getResults();

            * Indien je zeker bent dat de query maar 1 row als return heeft, raad ik aan gebruik te maken van de getFirst functie!


        * getError():
            Bij default: False, maar zodra een functie een fout heeft gemaakt, word dit opgeslagen als True.
            Via de getError functie kunnen we een controle uitvoeren om te kijken of onze query succesvol is verlopen.

            Voorbeeld controle:

                $user = DB::getInstance()->query('SELECT * FROM users');

                if( $user->getError() )
                {
                    echo 'No Users';
                }
                else
                {
                    var_dump( $user->getResults() );
                ]


        * getCount():
            Zondra een query succesvoll is uitgevoerd, worden de aantal regels met data geteld en
            bijgehouden om eventuele controle uit te voeren om te controleren of de query data terug gaf.


        * query():
            De functie query(); geeft de developer de mogelijkheid om een normale querys uit te laten voeren zonder afhankelijk te zijn van de query builder.

            Voorbeeld:

                $user = DB::getInstance()->query('SELECT * FROM users');

                if( $user->getError() )
                {
                    echo 'No Users';
                }
                else
                {
                    var_dump( $user->getResults() );
                ]


        * action():
            Deze method is uitgebouwd tot een query builder.
            Via deze functie is de developer in staat query`s op te maken zonder enige kennis te moeten hebben van MYSQL.
            Op zichzelfstaand, kan deze functie nogal ingewikkeld overkomen.
            Om deze reden zijn er 2 helper methods gemaakt: get() en delete().
            Deze functies dienen om het gebruik te vereenvoudige

            Om van deze querybuilder gebruik te kunnen maken zonder helper methods,
            zijn er 3 velden verplicht om de SQL query volledig op te bouwen:

            action($action, $table, $where = array());

            $action = Een string die defineerd wat er in de DB moet gebeuren: SELECT * of DELETE.
            $table = Een string die defineerd om welke tabel de query moet worden uitgevoerd.
            $where = Een array waarin 3 velden verplicht zijn om zo de WHERE clausule op te bouwen van de query:
                columnnaam, operator (mogelijke opties: =, >, <, >=, <= ), column waarde.

            Voorbeeld:

                $user = DB::getInstance()->action( 'SELECT *', 'users', array('user_id', '=', 1) );

                if(!$user->getCount())
                {
                    echo 'No Data';
                }
                else
                {
                    echo $user->getFirst()->username;
                }


//TODO: * get:


        * getAll:


        * delete:


        * insert:


        * update:


        * getFirst:
