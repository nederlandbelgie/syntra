<?php
/**
 * Class Details:
 *
 * User: Ministrare
 * Date: 11/02/2018
 * Time: 13:21
 *
 * General Helper Functions:
 *
 *  escape();
 *      Secures database input and output data for XSS (Cross-site scripting) attacks
 *      Use it after collecting data from Input values or after collecting data from the Database.
 *  Example:
 *      escape("string");
 *
 */

class helperFunctions
{
    /**
     * @param $string
     * @return string
     */
    public function escape($string){
        return htmlentities($string, ENT_QUOTES, 'UTF-8');
    }
}