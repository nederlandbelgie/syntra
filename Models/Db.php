<?php
/**
 * Class Details:
 *  User: Ministrare
 *  Date: 11/02/2018
 *  Time: 13:20
 *
 * DB Connection Class:
 *  Uses Global Settings in order to connect with the DB
 *
 *
 * 2 ways to access data:
 *  By query = DB::getInstance()->query( 'SELECT * FROM users' );
 *  or by query builder: action().
 *  Best practice: Use the get() or delete() method in order to make use of the Query builder.
 *
 *
 * Main Functions:
 *
 *  Public Static method: DB::getInstance();
 *      Allows us the get the instance if there is already a instantiate.
 *      Otherwise instantiate a new DB connection.
 *
 *  Public method getResults();
 *      Returns stored data as Object.
 *
 *      Usage Example:
 *          var_dump($user->getResults());
 *
 *
 *  Public method getError();
 *      Returns stored error.
 *      False by default.
 *
 *
 *  Public method getCount();
 *      Returns the amount of rows with data.
 *      This function serves it purpose to check if SQL query brings back any Data Rows
 *
 *
 *  Public method query();
 *      Execute SQL if prepared correctly.
 *      Sets _results and _count if execution is successful
 *      If failed, _error is set.
 *
 *      Usage Example:
 *          $user = DB::getInstance()->query( 'SELECT * FROM users' );
 *          if( $user->getCount() ){
 *              echo 'No Data';
 *          }else{
 *              var_dump($user->getResults());
 *          }
 *
 *
 *  Public method action($action, $table, $where = array());
 *      This method is not designed to be used without the Get or Delete methods.
 *
 *      This is a query builder used in multiple methods.
 *      This method handles the most common DB query`s in short notation.
 *
 *      If used, DB query`s can be build up as follow:
 *          $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
 *
 *      This method is being used in 2 other methods:
 *          get( $table, $where );
 *          delete( $table, $where );
 *
 *      Usage Example:
 *          $this->action( 'SELECT *', $table, $where_array )
 *
 *      Explanation variables:
 *          $table = string = "example_table";
 *          $where_array = array = array('example-table-column', '$operators like =, >, <, >=, <=', 'example-table-value')
 *      Like:
 *          $this->action( 'SELECT *', 'users', array('user_id','=',1) )
 *
 *
 *  Public method get( $table, $where );
 *      Get specific data from the Database based on a query builder... action();
 *      This method allows handling action methode with ease.
 *      The functions in combinations makes it that developers no longer have to think about easy SQL query`s.
 *      These functions are not there to completely replace the query() function, but to extend as a helper.
 *
 *      Usage Example:
 *          $groups = DB::getInstance()->get('example-table-name',array('example-table-column', '=', 1));
 *      Like:
 *          $groups = DB::getInstance()->get('premission_groups',array('group_id', '=', 1));
 *
 *
 *  Public method getAll($table);
 *      Get all data from the Database based on a query builder... action();
 *      This method allows handling action methode with ease.
 *      The functions in combinations makes it that developers no longer have to think about easy SQL query`s.
 *      These functions are not there to completely replace the query() function, but to extend as a helper.
 *
 *      Usage Example:
 *          $groups = DB::getInstance()->getAll('premission_groups');
 *          if(!$groups->getCount())
 *          {
 *              echo 'No Data';
 *          }
 *          else
 *          {
 *              foreach($groups->getResults() as $group)
 *              {
 *                  echo $group->group_name, "<br>";
 *              }
 *          }
 *
 *
 *  Public method delete( $table, $where );
 *      Delete data from the Database based on a query builder... action();
 *      This method allows handling action methode with ease.
 *      The functions in combinations makes it that developers no longer have to think about easy SQL query`s.
 *      These functions are not there to completely replace the query() function, but to extend as a helper.
 *
 *      Usage Example:
 *          $groups = DB::getInstance()->delete('example-table-name',array('example-table-column', '=', 1));
 *
 *      Like:
 *          $groups = DB::getInstance()->delete('premission_groups',array('group_id', '=', 1));
 *
 *
 *  Public method insert();
 *      Method to insert data and create new DB record by selecting a table and passing the record value`s true a array.
 *
 *      Usage Example:
 *          $userInsert = DB::getInstance()->insert('table', array( 'column' => 'new value');
 *
 *      Like:
 *          $userInsert = DB::getInstance()->insert('users', array(
 *              'username' => 'Admin',
 *              'password' => 'Admin',
 *              'salt' => 'salt',
 *              'name' => 'Lesley Forn',
 *              'permission_group' => 2,
 *          ));
 *
 *          if($userInsert)
 *          {
 *              // success!
 *          }
 *
 *
 *  Public method update();
 *      Update a record by ID by selecting a table and passing the record update value`s as a array.
 *      This means a DB Table always has a ID column as primary key.
 *
 *      Usage example:
 *          $userUpdate = DB::getInstance()->update('table', id, array( 'column' => 'updated value'));
 *
 *      Like:
 *          $userUpdate = DB::getInstance()->update('users', 1, array(
 *              'password' => 'Admin',
 *              'name' => 'admin'
 *          ));
 *
 *          if($userUpdate)
 *          {
 *              echo "success!";
 *          }
 *          else
 *          {
 *              echo 'Failed!';
 *          }
 *
 *
 *  Public method getFirst();
 *      If a query only returns 1 result, this function can be used instead of make used of foreach.
 *
 *      Usage Example:
 *          $group = DB::getInstance()->get('table',array('column', 'operator', 'value'));
 *          $group->getFirst()->column_value
 *
 *      like:
 *          $group = DB::getInstance()->get('premission_groups',array('group_id', '=', 1));
 *
 *          if(!$group->getCount())
 *          {
 *              echo 'No Data';
 *          }
 *          else
 *          {
 *              echo $group->getFirst()->group_name, "<br>";
 *          }
 *
 */

class DB
{
    private static $_instance = null;
    private $_pdo, $_query, $_results,
        $_count = 0, $_error = False,
        $_inserted_id = 0;

    /**
     * Connection constructor.
     * Constructs a DB connection and stores it in $_instance.
     * Only accessible true the getInstance method to make sure one connection is initiated.
     */
    private function __construct()
    {
        try
        {
            $this->_pdo =  new PDO('mysql:host='. Config::get( 'mysql/host' ) .';dbname='. Config::get( 'mysql/db' ), Config::get( 'mysql/username' ), Config::get( 'mysql/password' ));
            //echo 'Connected ';
        }
        catch( PDOException $e )
        {
            $this->_error = True;
            die( $e->getMessage() );
        }
    }

    /**
     * @return DB|null
     * Returns a DB instance or instantiate one if none exists
     */
    public static function getInstance()
    {
        if( !isset( self::$_instance ))
        {
            self::$_instance = new DB();
        }

        return self::$_instance;
    }

    /**
     * @return mixed
     */
    public function getResults()
    {
        return $this->_results;
    }

    /**
     * @return bool
     * Returns the error, false by default.
     */
    public function getError()
    {
        return $this->_error;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->_count;
    }

    /**
     * @param $sql
     * @param array $params
     * @return $this
     */
    public function query( $sql, $params = array() )
    {
        $this->_error = false;

        if($this->_query = $this->_pdo->prepare( $sql ))
        {
            $x= 1;

            if(count($params))
            {
                foreach ($params as $param)
                {
                    $this->_query->bindValue($x, $param);
                    $x++;
                }
            }

            if($this->_query->execute())
            {
                $this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
                $this->_count = $this->_query->rowCount();
                $this->_inserted_id = $this->_pdo->lastInsertId();
                //echo 'Success ';
            }
            else
            {
                $this->_error = true;
            }

        }

        return $this;
    }

    /**
     * @param $action
     * @param $table
     * @param array $where
     * @return $this
     */
    public function action($action, $table, $where = array())
    {
        if(count($where) === 3)
        {
            $operators = array('=','>','<','>=','<=');

            $field      = $where[0];
            $operator   = $where[1];
            $value      = $where[2];

            if(in_array($operator,$operators))
            {
                $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";

                if($this->query($sql, array($value)))
                {
                    return $this;
                }
            }
        }

        return $this;
    }

    /**
     * @param $table
     * @param $where
     * @return DB
     */
    public function get($table, $where)
    {
        return $this->action('SELECT *', $table, $where);
    }

    /**
     * @param $table
     * @return $this|bool
     */
    public function getAll($table)
    {
        $sql = "SELECT * FROM ". $table;

        if($this->query($sql))
        {
            return $this;
        }

        return false;
    }

    /**
     * @param $table
     * @param $where
     * @return DB
     */
    public function delete($table, $where)
    {
        return $this->action('DELETE', $table, $where);
    }

    /**
     * @param $table
     * @param array $fields
     * @return bool
     */
    public function insert($table, $fields = array())
    {
        if(count($fields))
        {
            $keys   = array_keys($fields);
            $values  = Null;
            $x      = 1;

            foreach($fields as $field)
            {
                $values .= '?';
                if($x < count($fields))
                {
                    $values .= ', ';
                }
                $x++;
            }

            $sql = "INSERT INTO ". $table ." (".implode(', ', $keys).") VALUES (".implode(',', array_fill(0, count($keys), '?')).")";

            if($this->query($sql, $fields)->getError())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $table
     * @param $id
     * @param $fields
     * @return bool
     */
    public function update($table, $id, $fields){
        $set = '';
        $x = 1;

        foreach ($fields as $name => $value)
        {
            $set .= "{$name} = ?";
            if($x < count($fields))
            {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";

        if(!$this->query($sql, $fields)->getError())
        {
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getFirst(){
        return $this->getResults()[0];
    }

    /**
     * @return int
     */
    public function getInsertedId(): int
    {
        return $this->_inserted_id;
    }
}