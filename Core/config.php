<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 6-2-2018
 * Time: 21:35
 */

session_start();

$GLOBALS['config'] = array(
    'mysql' => array(
        'host' => '127.0.0.1',

        'username'=> 'root',
        'password'=> '',
        'db'=> 'boekenbase'
    )
);

spl_autoload_register(function($class){
    require_once 'Models/'. $class .'.php';
});