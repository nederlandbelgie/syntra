<?php
/**
 * File Details:
 *  User: Ministrare
 *  Date: 11/02/2018
 *  Time: 13:19
 *
 * Initialisation file:
 *  sets Global Session, Config and Cookies.
 *  Also contains a autoloader for classes
 *
 * Includes all Controllers. (still basic setup, will change in the future.)
 */

require_once "Core/config.php";
require_once "Controllers/Controller.php";
require_once "Controllers/homeController.php";

