<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 6-2-2018
 * Time: 20:34
 */

class homeController extends Controller
{

    public function showHome() {
        $Model = new ViewModel();

        $Model->setPage("Home");
        $Model->setTitle("Books Manager | Homepage");

        $Model->renderHeader();
        $Model->renderBody();
        $Model->pageFooter();
    }

    public function showAdd(){
        $Model = new ViewModel();

        $Model->setPage("Add");
        $Model->setTitle("Books Manager | Add New Book");

        $Model->renderHeader();
        $Model->renderBody();
        $Model->pageFooter();
    }

    public function showEdit(){
        $Model = new ViewModel();

        $Model->setPage("Edit");
        $Model->setTitle("Books Manager | Edit Book Details");

        $Model->renderHeader();
        $Model->renderBody();
        $Model->pageFooter();
    }

    public function showDetails(){
        $Model = new ViewModel();

        $Model->setPage("Details");
        $Model->setTitle("Books Manager | Book Details");

        $Model->renderHeader();
        $Model->renderBody();
        $Model->pageFooter();
    }
    public function delete(){


    }
}